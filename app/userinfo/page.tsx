'use client';
import Image from 'next/image'
import { Table, Button, Modal, Checkbox, Label, TextInput } from 'flowbite-react';
import React from 'react';

interface USERTYPES{
    full_name: string;
    email: string;
    username: string;
}

export default function Home() {
    const [openModal, setOpenModal] = React.useState<boolean | undefined>(false);
    const [index, setIndex] = React.useState<number>(-1);
    const [formData, setFormData] = React.useState<USERTYPES>({
        full_name: '',
        email: '',
        username: ''
    });
    const [datas, setData] = React.useState<any>([
        {
            'id': 1,
            'full_name': 'Alhamdi',
            'username': 'alhamdi',
            'email': 'alhamdi@gmail.com'
        },
        {
            'id': 2,
            'full_name': 'Ferdiawan',
            'username': 'ferdiawan',
            'email': 'ferdiawan@gmail.com'
        },
        {
            'id': 3,
            'full_name': 'Bahri',
            'username': 'bahri',
            'email': 'bahri@gmail.com'
        }
    ]);

    const handleEditData = (item: any, index: number) => {
        setFormData({
            full_name: item.full_name,
            email: item.email,
            username: item.username,
        })
        setIndex(index)
        setOpenModal(true)
    }

    const handleSave = () => {
        if (index === -1) {
            let addData = [...datas]
            addData.splice(0, 0, formData)
            setData(addData)
        } else {
            let editData = [...datas]
            editData.splice(index, 1)
            editData.splice(index, 0, formData)
            setData(editData)
        }
        setOpenModal(false)
    }

    const handleDelete = (index: number) => {
        let deleteData = [...datas]
        deleteData.splice(index, 1)
        setData(deleteData)
    }

    return (
        <div className='p-5'>
            <h2 className="text-4xl font-extrabold dark:text-white mb-5" onClick={() => setOpenModal(true)}>Data Users</h2>

            <Button onClick={() => setOpenModal(true)}>Create Data</Button>
            <Table className='mt-4'>
                <Table.Head>
                    <Table.HeadCell>
                        Full Name
                    </Table.HeadCell>
                    <Table.HeadCell>
                        Email
                    </Table.HeadCell>
                    <Table.HeadCell>
                        Username
                    </Table.HeadCell>
                    <Table.HeadCell>
                        Action
                    </Table.HeadCell>
                </Table.Head>
                <Table.Body className="divide-y">
                    
                    {
                        datas.map((data:any, index: number) => (
                            <Table.Row className="bg-white dark:border-gray-700 dark:bg-gray-800" key={index}>
                                <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                    {data.full_name}
                                </Table.Cell>
                                <Table.Cell>
                                    {data.email}
                                </Table.Cell>
                                <Table.Cell>
                                    {data.username}
                                </Table.Cell>
                                <Table.Cell>
                                    <a
                                        className="font-medium pr-3 text-cyan-600 hover:underline dark:text-cyan-500"
                                        onClick={() => handleEditData(data,index)}
                                    >
                                        Edit
                                    </a>
                                    <a
                                        className="font-medium text-cyan-600 hover:underline dark:text-cyan-500"
                                        onClick={() => handleDelete(index)}
                                    >
                                        Delete
                                    </a>
                                </Table.Cell>
                            </Table.Row>
                        ))
                    }
                    
                </Table.Body>
            </Table>


            <Modal dismissible show={openModal} onClose={() => setOpenModal(false)}>
                <Modal.Header>{ index == -1 ? 'Create Data' : 'Update Data' }</Modal.Header>
                <Modal.Body>
                    <form className="w-full gap-4">
                        <div>
                            <div className="mb-2 block">
                            <Label
                                htmlFor="full_name"
                                value="Full Name"
                            />
                            </div>
                            <TextInput
                                onChange={(e:any) => setFormData({
                                    ...formData,
                                    full_name: e.target.value
                                })}
                                defaultValue={formData.full_name}
                                id="full_name"
                                placeholder="Full Name"
                                required
                                shadow
                                type="text"
                            />
                        </div>
                        <div>
                            <div className="mb-2 block">
                            <Label
                                htmlFor="email"
                                value="Email"
                            />
                            </div>
                            <TextInput
                                onChange={(e:any) => setFormData({
                                    ...formData,
                                    email: e.target.value
                                })}
                                defaultValue={formData.email}
                                id="email"
                                placeholder="Email"
                                required
                                shadow
                                type="email"
                            />
                        </div>
                        <div>
                            <div className="mb-2 block">
                            <Label
                                htmlFor="username"
                                value="Username"
                            />
                            </div>
                            <TextInput
                                onChange={(e:any) => setFormData({
                                    ...formData,
                                    username: e.target.value
                                })}
                                defaultValue={formData.username}
                                id="username"
                                placeholder="Username"
                                required
                                shadow
                                type="text"
                            />
                        </div>
                    </form>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={() => handleSave()}>Save</Button>
                    <Button color="gray" onClick={() => setOpenModal(false)}>
                        Close
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    )
}
